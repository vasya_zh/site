---
layout: post
title: Встреча 0x07 16-17 ноября 2019г
date: 2019-10-17 00:00:00
categories: announcement
short_description: Да, дата и место проведения известны
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Да, мы [определились](https://gyazo.com/d765a6ef5559c1d4405f5d7b042d8204) с датой проведения встречи [0x07](https://dc20e6.ru/meetup/0x07/) и это будет **вечер-ночь с 16 по 17 ноября 2019г** в **компьютерном клубе [HEADSHOT ЦЕНТР](https://www.headshot24.ru/headshot1)**

Давайте чуть подробнее опишу план встречи

- Встречаемся 16 ноября 2019г вечером (точное время будет чуть попозже как сформируется список докладов), читаем доклады
- "Закрываемся" на ночь в том же компьютерном клубе и начинается время "сажания кинескопов", воркшопов и чемпионата

Больше инфы на странице встречи [0x07: Вычислю по айпи или OSINT и другие способы получения и обработки данных из интернета. Игры, читы, баги и фичи :)](https://dc20e6.ru/meetup/0x07/) и в чате [@dc20e6](https://t.me/dc20e6)

P.S. Да, [запись докладов](https://www.youtube.com/channel/UCjZn5X7TyU2-INg1pFGu44w/playlists) будет, но вы лучше приходите-приезжайте! Мы вас ждем :)
P.P.S. Да, возможно и даже запись вокшопов будет, но это как повезет xD