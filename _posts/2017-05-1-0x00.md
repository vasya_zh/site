---
layout: post
title: 0x00 встреча
date: 2017-05-01 00:00:00
categories: news
short_description: 0x00 прошла успешно и продуктивно
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Привет,

C вами DEFCON 20e6 и сегодня расскажем как прошла [встреча в пятницу, 28 апреля, 2017](https://defcon-ulsk.ru/meetup/0x00/) 

А прошла она хорошо и краткие тезисы доступны тут: [ТУТ](https://defcon-ulsk.ru/meetup/0x00/meeting-notes/)
Много говорили, познакомились